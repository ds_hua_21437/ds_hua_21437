<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
	<div>
		<table>
			<tr>
				<th>Username</th>
				<th>Password</th>
			</tr>

			<c:forEach var="user" items="${user}">
				<tr>
					<td>${user.username}</td>
					<td>${user.password}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</sec:authorize>