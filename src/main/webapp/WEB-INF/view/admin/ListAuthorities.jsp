<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
	<div>
		<table>
			<tr>
				<th>Username</th>
				<th>Role</th>
			</tr>
			<c:forEach var="authority" items="${authority}">
				<tr>
					<td>${authority.username}</td>
					<td>${authority.authority}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</sec:authorize>