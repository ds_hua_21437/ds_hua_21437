<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
	<p></p>
	<form
		action="${pageContext.request.contextPath}/admin/AddUser">
		<input style="font-size: 20px" type="submit" value="Add a user">
	</form>
	<br>
	<form
		action="${pageContext.request.contextPath}/admin/DeleteUser">
		<input style="font-size: 20px" type="submit" value="Delete a user">
	</form>
	<br>
	<form
		action="${pageContext.request.contextPath}/admin/UpdateUser">
		<input style="font-size: 20px" type="submit" value="Update a user">
	</form>
	<br>	
	<form
		action="${pageContext.request.contextPath}/admin/ListUsers">
		<input style="font-size: 20px" type="submit" value="List Users">
	</form>	
	<br>	
	<form
		action="${pageContext.request.contextPath}/admin/ListAuthorities">
		<input style="font-size: 20px" type="submit" value="List Authorities">
	</form>
</sec:authorize>
