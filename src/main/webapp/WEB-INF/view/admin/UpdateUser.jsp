<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
	<div>
		<form:form action="ModifiedUser" method="POST">
			<p>Username: <input type="text" name="existUsername">
			<p>What do you want to update:</p>
			<select name="column">
				<option value="password">Password</option>
				<option value="role">Role</option>
			</select>
			<p>Enter new value: <input type="text" name="value">
			<p>Role options: 'ROLE_PERSONEL', 'ROLE_COMPANY'</p>
			<p><input type="submit">
		</form:form>
	</div>
</sec:authorize>