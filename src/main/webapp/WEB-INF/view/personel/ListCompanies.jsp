<sec:authorize access="hasAnyRole('ROLE_USER')">
	<div>
		<table>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Subject</th>
			</tr>

			<c:forEach var="companies" items="${companies}">
				<tr>
					<td>${companies.id}</td>
					<td>${companies.name}</td>
					<td>${companies.subject}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</sec:authorize>