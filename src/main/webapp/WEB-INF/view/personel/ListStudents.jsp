<sec:authorize access="hasAnyRole('ROLE_USER')">
	<div>
		<table>
			<tr>
				<th>ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>CurStudYear</th>
				<th>Classes Left</th>
				<th>Eligible For Internship</th>
				<th>Number Of Applications</th>
			</tr>

			<c:forEach var="students" items="${students}">
				<tr>
					<td>${students.id}</td>
					<td>${students.firstName}</td>
					<td>${students.lastName}</td>
					<td>${students.curStudYear}</td>
					<td>${students.classesLeft}</td>
					<td>${students.eligible}</td>
					<td>${students.noApplications}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</sec:authorize>