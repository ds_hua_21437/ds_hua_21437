<sec:authorize access="hasAnyRole('ROLE_USER')">
	<div>
		<table>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Company ID</th>
			</tr>

			<c:forEach var="positions" items="${positions}">
				<tr>
					<td>${positions.id}</td>
					<td>${positions.name}</td>
					<td>${positions.companyId}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</sec:authorize>