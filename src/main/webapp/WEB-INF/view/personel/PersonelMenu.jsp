<sec:authorize access="hasAnyRole('ROLE_USER')">
	<p></p>
	<form action="${pageContext.request.contextPath}/personel/ListStudents">
		<input style="font-size: 20px" type="submit" value="List Students">
	</form>
	<br>
	<form
		action="${pageContext.request.contextPath}/personel/ListApplications">
		<input style="font-size: 20px" type="submit" value="List Applications">
	</form>
	<br>
	<form
		action="${pageContext.request.contextPath}/personel/ListCompanies">
		<input style="font-size: 20px" type="submit" value="List Companies">
	</form>
	<br>
	<form
		action="${pageContext.request.contextPath}/personel/ListPositions">
		<input style="font-size: 20px" type="submit" value="List Positions">
	</form>
	<br>
	<form
		action="${pageContext.request.contextPath}/personel/UpdateStudentEligibility">
		<input style="font-size: 20px" type="submit"
			value="Update Student Eligibility">
	</form>
	<br>
	<form
		action="${pageContext.request.contextPath}/personel/UpdateApplication">
		<input style="font-size: 20px" type="submit"
			value="Update An Application">
	</form>
	<br>
	<form
		action="${pageContext.request.contextPath}/personel/DeleteCompany">
		<input style="font-size: 20px" type="submit" value="Delete A Company">
	</form>
	<br>
	<form
		action="${pageContext.request.contextPath}/personel/AddPosition">
		<input style="font-size: 20px" type="submit" value="Add A Position">
	</form>

</sec:authorize>
