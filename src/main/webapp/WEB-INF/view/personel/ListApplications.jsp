<sec:authorize access="hasAnyRole('ROLE_USER')">
	<div>
		<table>
			<tr>
				<th>ID</th>
				<th>Student</th>
				<th>Position_ID</th>
				<th>Progress</th>

			</tr>

			<c:forEach var="applications" items="${applications}">
				<tr>
					<td>${applications.id}</td>
					<td>${applications.student}</td>
					<td>${applications.position_id}</td>
					<td>${applications.progress}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</sec:authorize>