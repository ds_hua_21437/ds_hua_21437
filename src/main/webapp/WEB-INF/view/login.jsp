<div>
	<h3>Login</h3>

	<form:form action="${pageContext.request.contextPath}/authUser" method="POST">
		<c:if test="${param.error != null}">
			<div>
				<p>Invalid username/password!</p>
			</div>
		</c:if>
		
		<c:if test="${param.logout != null}">
			<div>
				<div>Bye!</div>
				<p>You have been logged out!</p>
			</div>
		</c:if>
		<div>
			<label>User Name:</label> <input type="text" name="username" />
		</div>
		<div>
			<label>Password:</label> <input type="password" name="password" />
		</div>
		<div>
			<input type="submit" value="Login" />
		</div>
	</form:form>

</div>