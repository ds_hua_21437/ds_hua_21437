package gr.hua.dit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Authority")
@Entity
@Table(name = "authorities")
public class Authority {
		//Fields
		@Id
		@Column(name = "username", unique = true, nullable = false)
		private String username;

		@Column(name = "authority", nullable = false)
		private String authority;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getAuthority() {
			return authority;
		}

		public void setAuthority(String authority) {
			this.authority = authority;
		}

		@Override
		public String toString() {
			return "Authority [username=" + username + ", authority=" + authority + "]";
		}
		
}
