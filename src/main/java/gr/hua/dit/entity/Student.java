package gr.hua.dit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Student")
@Entity
@Table(name = "students")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "curStudYear")
	private int curStudYear;
	
	@Column(name = "classesLeft")
	private int classesLeft;
	
	@Column(name = "eligible_for_internship")
	private boolean eligible;
	
	@Column(name = "no_applications")
	private int noApplications;
	
	public Student() {
		
	}
		
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getCurStudYear() {
		return curStudYear;
	}

	public void setCurStudYear(int curStudYear) {
		this.curStudYear = curStudYear;
	}

	public int getClassesLeft() {
		return classesLeft;
	}

	public void setClassesLeft(int classesLeft) {
		this.classesLeft = classesLeft;
	}

	public boolean isEligible() {
		return eligible;
	}

	public void setEligible(boolean eligible) {
		this.eligible = eligible;
	}
	

	public int getNoApplications() {
		return noApplications;
	}

	public void setNoApplications(int noApplications) {
		this.noApplications = noApplications;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", curStudYear="
				+ curStudYear + ", classesLeft=" + classesLeft + ", eligible=" + eligible + ", noApplications="
				+ noApplications + "]";
	}

}