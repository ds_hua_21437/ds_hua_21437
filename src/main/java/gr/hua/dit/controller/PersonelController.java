package gr.hua.dit.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import gr.hua.dit.entity.Student;
import gr.hua.dit.service.PersonelService;
import gr.hua.dit.entity.Application;
import gr.hua.dit.entity.Company;
import gr.hua.dit.entity.InternshipPosition;

@Controller
@RequestMapping("/personel")
public class PersonelController {

	// inject the personel service
	@Autowired
	private PersonelService personelService;

	@RequestMapping("/PersonelMenu")
	public String PersonelMenu() {
		return "personel/PersonelMenu";
	}

	@RequestMapping("/ListStudents")
	public String listStudents(Model model) {

		// get students from dao
		List<Student> students = personelService.listStudents();

		// add students to the model
		model.addAttribute("students", students);

		return "personel/ListStudents";
	}

	@RequestMapping("/ListApplications")
	public String listApplications(Model model) {

		// get applications from dao
		List<Application> applications = personelService.listApplications();

		// add applications to the model
		model.addAttribute("applications", applications);

		return "personel/ListApplications";
	}
	
	@RequestMapping("/ListCompanies")
	public String listCompanies(Model model) {

		// get companies from dao
		List<Company> companies = personelService.listCompanies();

		// add companies to the model
		model.addAttribute("companies", companies);

		return "personel/ListCompanies";
	}
	
	@RequestMapping("/ListPositions")
	public String listPositions(Model model) {

		// get positions from dao
		List<InternshipPosition> positions = personelService.listPositions();

		// add positions to the model
		model.addAttribute("positions", positions);

		return "personel/ListPositions";
	}

	@GetMapping("/UpdateStudentEligibility")
	public String UpdateStudent() {
		return "/personel/UpdateStudentEligibility";
	}
	
	@PostMapping("/UpdatedStudent")
	public String UpdatedStudent(HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		Boolean eligible = Boolean.parseBoolean(request.getParameter("eligible"));
		personelService.updateEligibility(id, eligible);
		return "redirect:/personel/UpdateStudentEligibility";
	}
	
	@GetMapping("/UpdateApplication")
	public String UpdateApplication() {
		return "/personel/UpdateApplication";
	}
	
	@PostMapping("/UpdatedApplication")
	public String ModifiedUser(HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		String progress = request.getParameter("progress");
		personelService.updateApplication(id, progress);
		return "redirect:/personel/UpdateApplication";
	}
	
	@GetMapping("/DeleteCompany")
	public String DeleteCompany() {
		return "/personel/DeleteCompany";
	}
	
	@PostMapping("/DeletedCompany")
	public String DeleteCompany(HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("company_id"));

		personelService.deleteCompany(id);
		return "redirect:/personel/DeleteCompany";
	}
	
	@GetMapping("/AddPosition")
	public String AddPosition() {
		return "/personel/AddPosition";
	}
	
	@PostMapping("/AddedPosition")
	public String AddedPosition(HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		int companyId = Integer.parseInt(request.getParameter("company_id"));
		personelService.addPosition(id ,name ,companyId);
		return "redirect:/personel/AddPosition";
	}
}