package gr.hua.dit.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import gr.hua.dit.service.CompanyService;

@Controller
@RequestMapping("/company")
public class CompanyController {

	// inject the company service
	@Autowired
	private CompanyService companyService;

	@RequestMapping("/CompanyMenu")
	public String CompanyMenu() {
		return "company/CompanyMenu";
	}

	@GetMapping("/AddCompany")
	public String AddCompany() {
		return "/company/AddCompany";
	}

	@PostMapping("/AddedCompany")
	public String AddedCompany(HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException {

		String name = request.getParameter("name");
		String subject = request.getParameter("subject");
		companyService.addCompany(name, subject);
		return "redirect:/company/AddCompany";
	}
}