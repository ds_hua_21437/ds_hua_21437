package gr.hua.dit.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import gr.hua.dit.entity.Authority;
import gr.hua.dit.entity.User;
import gr.hua.dit.service.AdminService;

@Controller
@RequestMapping("/admin")
public class AdminController {

	// inject the User DAO
	@Autowired
	private AdminService AdminService;

	@RequestMapping("/AdminMenu")
	public String AdminMenu() {
		return "admin/AdminMenu";
	}

	@GetMapping("/AddUser")
	public String AddUser() {
		return "admin/AddUser";
	}

	@PostMapping("/SaveUser")
	public String SaveUser(HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String role = request.getParameter("role");
		short enabled = 1;
		AdminService.AddUser(username, password, enabled,role);
		return "redirect:/admin/AddUser";
	}

	@GetMapping("/DeleteUser")
	public String DeleteUser() {
		return "admin/DeleteUser";
	}

	@PostMapping("/RemoveUser")
	public String DeleteUser(HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException {
		String username = request.getParameter("username");
		if (!username.equals("null")) {
			AdminService.DeleteUser(username);
		}
		return "redirect:/admin/DeleteUser";
	}

	@GetMapping("/UpdateUser")
	public String UpdateUser() {
		return "admin/UpdateUser";
	}

	@PostMapping("/ModifiedUser")
	public String ModifiedUser(HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException {
		String column = request.getParameter("column");
		String newValue = request.getParameter("value");
		String existUser = request.getParameter("existUsername");
		AdminService.UpdateUser(column, newValue, existUser);
		return "redirect:/admin/UpdateUser";
	}

	@RequestMapping("/ListUsers")
	public String listUsers(Model model) {
		// get Users from DAO
		List<User> users = AdminService.listUsers();

		// add the Users to the model
		model.addAttribute("user", users);

		return "admin/ListUsers";
	}

	@RequestMapping("/ListAuthorities")
	public String listAuthorities(Model model) {
		// get Authorities from DAO
		List<Authority> authorities = AdminService.listAuthorities();

		// add the authorities to the model
		model.addAttribute("authority", authorities);

		return "admin/ListAuthorities";
	}

}
