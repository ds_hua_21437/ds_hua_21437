package gr.hua.dit.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gr.hua.dit.DAO.AdminDAO;
import gr.hua.dit.entity.Authority;
import gr.hua.dit.entity.User;

@Service
public class AdminServiceImpl implements AdminService{

	@Autowired
	private AdminDAO AdminDAO;
	
	@Override
	@Transactional
	public List<User> listUsers() {
		return AdminDAO.listUsers();
	}

	@Override
	@Transactional
	public void AddUser(String username, String password, short enabled, String role) {
		AdminDAO.AddUser(username,password,enabled,role);
	}

	@Override
	@Transactional
	public void DeleteUser(String username) {
		AdminDAO.DeleteUser(username);
	}

	@Override
	@Transactional
	public List<Authority> listAuthorities() {
		return AdminDAO.listAuthorities();
	}

	@Override
	@Transactional
	public void UpdateUser(String column, String newValue, String username) {
		AdminDAO.UpdateUser(column, newValue, username);
	}

}
