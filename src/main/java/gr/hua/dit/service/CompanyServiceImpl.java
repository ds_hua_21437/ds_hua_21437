package gr.hua.dit.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gr.hua.dit.DAO.CompanyDAO;

@Service
public class CompanyServiceImpl implements CompanyService{
	@Autowired
	private CompanyDAO CompanyDAO;
	
	@Override
	@Transactional
	public void addCompany(String name, String subject) {
		CompanyDAO.addCompany(name, subject);		
	}

}
