package gr.hua.dit.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gr.hua.dit.DAO.PersonelDAO;
import gr.hua.dit.entity.Application;
import gr.hua.dit.entity.Company;
import gr.hua.dit.entity.InternshipPosition;
import gr.hua.dit.entity.Student;

@Service
public class PersonelServiceImpl implements PersonelService {
	// inject the PersonelDAO
	@Autowired
	private PersonelDAO PersonelDAO;
	
	@Override
	@Transactional
	public List<Student> listStudents() {
		return PersonelDAO.listStudents();
	}
	
	@Override
	@Transactional
	public void updateEligibility(int id, boolean eligible) {
		PersonelDAO.updateEligibility(id, eligible);
	}

	@Override
	@Transactional
	public List<Company> listCompanies() {
		return PersonelDAO.listCompanies();
	}

	@Override
	@Transactional
	public void deleteCompany(int id) {
		PersonelDAO.deleteCompany(id);
	}

	@Override
	@Transactional
	public List<InternshipPosition> listPositions() {
		return PersonelDAO.listPositions();
	}

	@Override
	@Transactional
	public void addPosition(int id, String name, int companyId) {
		PersonelDAO.addPosition(id, name, companyId);		
	}

	@Override
	@Transactional
	public List<Application> listApplications() {
		return PersonelDAO.listApplications();
	}

	@Override
	@Transactional
	public void updateApplication(int id, String progress) {
		PersonelDAO.updateApplication(id, progress);
	}

}
