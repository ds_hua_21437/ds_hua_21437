package gr.hua.dit.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Repository;

import gr.hua.dit.entity.Authority;
import gr.hua.dit.entity.User;

@Repository
public class AdminDAOImpl implements AdminDAO {

	// inject the session factory
	@Autowired
	private SessionFactory sessionFactory;

	
	@Override
	public void AddUser(String username, String password, short enabled,String role) {
		// create a new session
		Session currentSession = sessionFactory.getCurrentSession();
		// create a User object using constructor
		password = BCrypt.hashpw(password, BCrypt.gensalt());
		User user = new User(username, password, enabled);
		// add user to the table
		Authority authority = new Authority();
		authority.setUsername(username);
		authority.setAuthority(role);
		currentSession.save(user);
		currentSession.save(authority);
	}

	@Override
	public void DeleteUser(String username) {
		// create a new session
		Session currentSession = sessionFactory.getCurrentSession();
		
		Query<Authority> authorities = currentSession.createQuery("from Authority u where u.username =?1",Authority.class).setParameter(1,username);;
		List<Authority> result = authorities.getResultList();
		currentSession.delete(result.get(0));
		
		Query<User> user = currentSession.createQuery("from User c where c.username= ?1", User.class).setParameter(1,username);
		List<User> result1 = user.getResultList();
		currentSession.delete(result1.get(0));
	}

	@Override
	public List<User> listUsers() {
		// create a new session
		Session currentSession = sessionFactory.getCurrentSession();

		Query<User> query = currentSession.createQuery("from User", User.class);

		// get all users from the table and add them to a list
		List<User> users = query.getResultList();

		// return the results
		return users;
	}
	
	@Override
	public void UpdateUser(String column, String newValue, String username) {
		// create a new session
		Session currentSession = sessionFactory.getCurrentSession();
		String q = null;
		//JSP column
		if (column.equals("password")) {
			newValue = BCrypt.hashpw(newValue, BCrypt.gensalt());
			q = "update User set password = ?1 where username = ?2";
		} else if (column.equals("role")) {
			q = "update Authorities set authority = ?1 where username = ?2";
		}
		currentSession.createQuery(q).setParameter(1, newValue).setParameter(2, username).executeUpdate();
	}

	@Override
	public List<Authority> listAuthorities() {
		// create a new session
		Session currentSession = sessionFactory.getCurrentSession();

		Query<Authority> query = currentSession.createQuery("from Authority", Authority.class);

		// get all users from the table and add them to a list
		List<Authority> authorities = query.getResultList();

		// return the results
		return authorities;
	}

}
