package gr.hua.dit.DAO;

import gr.hua.dit.entity.Application;
import gr.hua.dit.entity.Company;
import gr.hua.dit.entity.InternshipPosition;
import gr.hua.dit.entity.Student;

import java.util.List;

public interface PersonelDAO {
	//For Students
    public List<Student> listStudents();
    public void updateEligibility(int id, boolean eligible);
	
    //For Applications
	public List<Application> listApplications();
	public void updateApplication(int id, String progress);
    
    //For Companies
    public List<Company> listCompanies();
	public void deleteCompany(int id);
	
	//For Internship Positions
	public List<InternshipPosition> listPositions();
	public void addPosition(int id, String name, int companyId);

}
