package gr.hua.dit.DAO;

import java.util.List;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import gr.hua.dit.entity.Application;
import gr.hua.dit.entity.Company;
import gr.hua.dit.entity.InternshipPosition;
import gr.hua.dit.entity.Student;

@Repository
public class PersonelDAOImpl implements PersonelDAO{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Student> listStudents() {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		// create a query
		Query<Student> query = currentSession.createQuery("from Student", Student.class);
		// execute the query and get the results list
		List<Student> students = query.getResultList();
		// return the results
		return students;
	}

	//Updates the Student's eligibility for internship
	@Override
	public void updateEligibility(int id, boolean eligible) {
		Session currentSession = sessionFactory.getCurrentSession();
		String queryString = null;
		
		queryString = "update Student set eligible = ?1 where id = ?2";
		
		currentSession.createQuery(queryString).setParameter(1, eligible).setParameter(2, id).executeUpdate();
	}

	@Override
	public List<Company> listCompanies() {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		// create a query
		Query<Company> query = currentSession.createQuery("from Company", Company.class);
		// execute the query and get the results list
		List<Company> companies = query.getResultList();
		// return the results
		return companies;
	}

	@Override
	public void deleteCompany(int id) {
		//create a new session
    	Session currentSession = sessionFactory.getCurrentSession();
    	
		Query<Company> company = currentSession.createQuery("from Company c where c.id= ?1", Company.class).setParameter(1,id);
		List<Company> result1 = company.getResultList();
		currentSession.delete(result1.get(0));	
		
	}

	@Override
	public List<InternshipPosition> listPositions() {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		// create a query
		Query<InternshipPosition> query = currentSession.createQuery("From InternshipPosition", InternshipPosition.class);
		// execute the query and get the results list
		List<InternshipPosition> positions = query.getResultList();
		// return the results
		return positions;
	}

	@Override
	public void addPosition(int id, String name, int companyId) {
		Session currentSession = sessionFactory.getCurrentSession();
		InternshipPosition internshipPosition = new InternshipPosition();
		internshipPosition.setId(id);
		internshipPosition.setName(name);
		internshipPosition.setCompanyId(companyId);
		
		currentSession.save(internshipPosition);
	}

	@Override
	public List<Application> listApplications() {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		// create a query
		Query<Application> query = currentSession.createQuery("From Application", Application.class);
		// execute the query and get the results list
		List<Application> applications = query.getResultList();
		// return the results
		return applications;
	}

	@Override
	public void updateApplication(int id, String progress) {
		Session currentSession = sessionFactory.getCurrentSession();
		String queryString = null;
		
		queryString = "update Application set progress = ?1 where id = ?2";
		
		currentSession.createQuery(queryString).setParameter(1, progress).setParameter(2, id).executeUpdate();
	}
}
