package gr.hua.dit.DAO;

import java.util.List;
import gr.hua.dit.entity.Authority;
import gr.hua.dit.entity.User;

public interface AdminDAO {
	//Users
	public List<User> listUsers();
	public void AddUser(String username,String password, short enabled,String role);	
	public void DeleteUser(String username);
	void UpdateUser(String column, String newValue, String username);
	
	//Authorities
	public List<Authority> listAuthorities();
}
