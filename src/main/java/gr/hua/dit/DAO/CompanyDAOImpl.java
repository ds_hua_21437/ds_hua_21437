package gr.hua.dit.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import gr.hua.dit.entity.Company;

@Repository
public class CompanyDAOImpl implements CompanyDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addCompany(String name, String subject) {
		Session currentSession = sessionFactory.getCurrentSession();
		Company company = new Company();
		company.setName(name);
		company.setSubject(subject);

		currentSession.save(company);
	}

}
